from flask import Flask, request

app = Flask(__name__)

@app.route("/")
def index():
   return "Hello BoB from hw33ni"

@app.route("/add")
def add():
   a = request.args.get("a", type=int)
   b = request.args.get("b", type=int)
   if a==None or b==None: return "invalid input!"
   else: return str(a+b)

@app.route("/sub")
def sub():
   a = request.args.get("a", type=int)
   b = request.args.get("b", type=int)
   if a==None or b==None: return "invalid input!"
   else: return str(a-b)

if __name__ == "__main__":
   app.run(host="0.0.0.0",port=8049, debug="on")
